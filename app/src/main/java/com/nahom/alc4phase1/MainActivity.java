package com.nahom.alc4phase1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button about_ALC_button;
    private Button my_profile_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.main_title);
         about_ALC_button = (Button) findViewById(R.id.about_alc);
         my_profile_button = (Button) findViewById(R.id.my_profile);

        about_ALC_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aboutIntent = new Intent(view.getContext(), AboutALCActivity.class);
                startActivity(aboutIntent);
            }
        });

        my_profile_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myProfileIntent = new Intent(view.getContext(), MyProfileActivity.class);
                startActivity(myProfileIntent);
            }
        });
    }

}
