package com.nahom.alc4phase1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class MyProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setListContent();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void setListContent() {
        final RecyclerView profile_detail_recyclerview = (RecyclerView) findViewById(R.id.profile_detail);
        final LinearLayoutManager profile_detail_layout_manager = new LinearLayoutManager(this);
        profile_detail_recyclerview.setLayoutManager(profile_detail_layout_manager);
        List<ProfileDetailInfo> details = DataManager.getInstance().getProfileDetailList();
        final ProfileDetailAdapter profile_detail_adapter = new ProfileDetailAdapter(this,details);
        profile_detail_recyclerview.setAdapter(profile_detail_adapter);
    }
}