package com.nahom.alc4phase1;

public final class ProfileDetailInfo {
    private String mlabel;
    private String mvalue;

    public ProfileDetailInfo(String label, String value) {
        this.mlabel = label;
        this.mvalue = value;

    }

    public String getLabel() {
        return mlabel;
    }

    public void setLabel(String label) {
        mlabel = label;
    }

    public String getValue() {
        return mvalue;
    }

    public void setValue(String value) {
        mvalue = value;
    }
}
