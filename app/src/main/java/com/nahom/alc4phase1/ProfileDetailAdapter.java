package com.nahom.alc4phase1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ProfileDetailAdapter extends RecyclerView.Adapter<ProfileDetailAdapter.ViewHolder> {
    private final Context _context;
    private final LayoutInflater layout_inflater;
    private final List<ProfileDetailInfo> _detail_info;

    public ProfileDetailAdapter(Context context, List<ProfileDetailInfo> detail_info) {
        this._context = context;
        this._detail_info = detail_info;
        layout_inflater = LayoutInflater.from(_context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View list_item_view = layout_inflater.inflate(R.layout.profile_list_item,viewGroup,false);
        return new ViewHolder(list_item_view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ProfileDetailInfo detail = _detail_info.get(position);
        viewHolder.label_text_view.setText(detail.getLabel());
        viewHolder.value_text_view.setText(detail.getValue());
    }

    @Override
    public int getItemCount() {
        return _detail_info.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        public final TextView label_text_view;
        public final TextView value_text_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            label_text_view = (TextView) itemView.findViewById(R.id.label);
            value_text_view = (TextView) itemView.findViewById(R.id.value);
        }
    }
}
