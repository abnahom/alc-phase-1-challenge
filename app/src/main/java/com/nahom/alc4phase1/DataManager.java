package com.nahom.alc4phase1;

import java.util.ArrayList;
import java.util.List;

public class DataManager {
    private static DataManager ourInstance = null;
    private List<ProfileDetailInfo> detail_info_list= new ArrayList<>();

    public static DataManager getInstance() {
        if(ourInstance == null) {
            ourInstance = new DataManager();
            ourInstance.initializeList();
        }
        return ourInstance;
    }
    public List<ProfileDetailInfo> getProfileDetailList() {
        return detail_info_list;
    }
    // Optional for future use
    public int findProfileDetail(ProfileDetailInfo detailInfo) {
        for(int index = 0; index < detail_info_list.size(); index++) {
            if(detailInfo.equals(detail_info_list.get(index)))
                return index;
        }
        return -1;
    }
    // Optional for future use
    public void removeProfileDetail(int index) {
        detail_info_list.remove(index);
    }

    private DataManager() {
    }

    private void initializeList() {
        final DataManager dm = getInstance();
        detail_info_list.add( new ProfileDetailInfo("Track:","Android"));
        detail_info_list.add( new ProfileDetailInfo("Country:","Ethiopia"));
        detail_info_list.add( new ProfileDetailInfo("Email:","anahomb@gmail.com"));
        detail_info_list.add( new ProfileDetailInfo("Phone Number:","+251-911-399-631"));
        detail_info_list.add( new ProfileDetailInfo("Slack Username:","@Nahom Ab"));


    }
}
